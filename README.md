![picture](https://i.imgur.com/bKh3Im0.png)

Unofficial Disney+ application, Open source and multi-platform for all platforms to use.

Enjoy all your Disney+ content just like on the web version all wrapped up into a desktop application!

 &nbsp;&nbsp;&nbsp;&nbsp;

  You can install Youtube from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/disneyplus/)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/disneyplusdesktop/application/-/releases)

 ### Author
  * Corey Bruce